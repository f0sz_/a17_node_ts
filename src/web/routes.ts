import Helper from './helper';
import { Response, Request } from 'express';
import * as express from 'express';
import db from './database';
const helper = new Helper();

class ApiRouter {
  router: express.Router;
  constructor() {
    this.router = express.Router();
    this.init();
  }

  init() {
    this.router.post('/validate', this.validate);
    this.router.post('/reset-password', this.resetPassword);
  }

  public validate(req: Request, res: Response) {
    // checking only for email, since verification_code are not filled in
    // cause i don't have the apex class :x
    if (req.body.email && req.body.password) {
      const email = req.body.email;

      db.task('test', async t => {
        try {
          const user = await helper.getUser(t, email);

          const officePromise = helper.getOffice(t, 'Office Krakow');
          const entryPromise = helper.getEntry(t, user.sfid);
          const [office, entries] = await Promise.all([officePromise, entryPromise]);

          if (entries.length > 0) {
            return await helper.updateEntry(t, entries[0].sfid);
          } else {
            return await helper.insertEntry(t, user, office.sfid);
          }

        } catch (e) {
          res.send({
            success: false,
            response_date: new Date(),
            payload: e,
            errorMessage: e.message || 'Unknown error'
          })
        }
      })
        .then(entry =>
          res.send({
            success: true,
            response_date: new Date(),
            payload: entry,
            errorMessage: ''
          }))

    } else {
      res.send({
        success: false,
        response_date: new Date(),
        payload: null,
        errorMessage: 'You have to send email and password'
      });
    }
  }

  public resetPassword(req: Request, res: Response) {
    res.send("Not working yet, since i don't have apex class written :-(");
  }

}

const routes = new ApiRouter();
routes.init();

export default routes.router;