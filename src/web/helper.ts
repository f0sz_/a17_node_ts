import db from './database';

class Helper {

  constructor() {
    this.getUser;
    this.getEntry;
    this.getOffice;
    this.updateEntry;
    this.insertEntry;
  }

  public getUser(t: any, email: String) {
    return t.one('select * from salesforce.User where email=$1 and isactive=True', [email]);
  }

  public getEntry(t: any, userId: String) {
    return t.any('select * from salesforce.Presence_Entry__c where employee__c=$1 and start_date__c::Date=Now()::Date limit 1', [userId]);
  }

  public updateEntry(t: any, entryId: String) {
    return t.one('Update salesforce.Presence_Entry__c set end_date__c=Now() where sfid=$1 returning *', [entryId]);
  }

  public getOffice(t: any, name: String) {
    return t.one('Select * from salesforce.Account where name=$1 limit 1', ['Office Krakow'])
  }

  public insertEntry(t: any, user: any, officeId: String) {
    return t.one('insert into salesforce.Presence_Entry__c(name, office__c, employee__c, start_date__c, end_date__c) VALUES($1, $2, $3, Now(), Now()) returning *',
      [`${user.name} entry`, officeId, user.sfid])
  }

}

export default Helper;