import {IMain, IDatabase} from 'pg-promise';
import * as pgPromise from 'pg-promise';

const pgp = pgPromise();

pgp.pg.defaults.ssl = true;

const db:any = pgp(process.env.DATABASE_URL);

export default db;

