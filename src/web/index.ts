import * as dotenv from 'dotenv';
dotenv.config();
import * as express from 'express';
import * as path from 'path';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import routes from './routes';

const app = express();

app.use('/static', express.static(path.join(__dirname, 'public')));
app.use(cors());
app.use(bodyParser.json());
app.set('port', process.env.PORT || 5000);

app.use('/api/v1', routes);

app.listen(app.get('port'), () => {
  console.log('App listening on port: ' + app.get('port'));
})